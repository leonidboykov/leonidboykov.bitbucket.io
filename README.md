# Bitbucket Pages redirects

This repo contains HTML files with redirects to the main Netlify static-site [deployment](https://leonidboykov.com).

## Adding redirects

To redirect a Bitbucket Pages project `project_name` to a `https://new_url/`, just add a `project_name/index.html` file with the following content

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Redirecting to https://new_url/</title>
    <meta http-equiv="refresh" content="0; url=https://new_url/">
    <link rel="canonical" href="https://new_url/">
  </head>
</html>
```